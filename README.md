# SingTel Nokia G240 Guide

<!-- Referenced from https://gitlab.com/othneildrew/Best-README-Template/ -->
<a name="readme-top"></a>
<!--
-->

[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/asyraf_rahim/singtel_nokia_onr">
    <img src="images/logo.png" alt="Logo" width="200" height="120">
  </a>

<h3 align="center">Nokia G240 ONR Guide</h3>

  <p align="center">
    A guide to unlock access to certain features in SingTel-issued G240 Nokia Optical Network Routers
    <br />
    <a href="https://gitlab.com/asyraf_rahim/singtel_nokia_onr"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/asyraf_rahim/singtel_nokia_onr">View Demo</a>
    ·
    <a href="https://gitlab.com/asyraf_rahim/singtel_nokia_onr/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/asyraf_rahim/singtel_nokia_onr/issues">Request</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#guide">Guide</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

A guide created from reverse-engineering the ONR to obtain privileged access to SingTel-issued Nokia G240 Optical Network Routers.

<!-- Here's a blank template to get started: To avoid retyping too much info. Do a search and replace with your text editor for the following: `gitlab_username`, `repo_name`, `twitter_handle`, `linkedin_username`, `email_client`, `email`, `project_title`, `project_description` -->

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

**DISCLAIMER: THIS GUIDE ONLY WORKS WITH SINGTEL-ISSUED NOKIA G240 OPTICAL NETWORK ROUTERS (ONR)**

**WARNING: FOLLOWING THIS GUIDE REQUIRES YOU TO HARD RESET YOUR NOKIA G240 ONR TO FACTORY SETTINGS**

Press the Reset button by using a needle-like object and hold the button down for more than 10 seconds. 
If the LED indicator turns off and on, your ONR has been successfully restored to factory default settings.

<!-- Guide -->
## Guide

1. Find the serial number of the ONR. It should be found on the underside of the ONR.
<div align="center">
 <img src="images/onr_example.png" alt="Logo" width="300" height="200">
</div>

2. Note down the characters after "ALCL", from right to left. This will be your admin password.

3. Go to the ONR admin GUI on a browser by typing in the IP address (in our case it is 192.168.1.254)

4. Log in using the username `support` and the password that you have noted down in Step 2.

5. If the process was successful, you should be able to configure most of the settings that are available on the Nokia ONR.

> **Warning:** There are certain options when using the "Support" user that are only understood by the ISP operator, therefore discretion is advised.

<!-- ROADMAP -->
## Roadmap

- [X] 

See the [open issues](https://gitlab.com/asyraf_rahim/singtel_nokia_onr/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Muhammad Asyraf Bin Abdul Rahim - work@officialasyraf.info

Project Link: [https://gitlab.com/asyraf_rahim/singtel_nokia_onr](https://gitlab.com/asyraf_rahim/singtel_nokia_onr)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments


<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/muhammad-asyraf-bin-abdul-rahim/
[product-screenshot]: images/screenshot.png
